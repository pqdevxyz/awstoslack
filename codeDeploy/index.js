const sendToSlack = require('base/base.js');
const colors = require('base/colors.js');
const webhooks = require('base/webhooks.js');

exports.handler = (event, context, callback) => {
    let color = colors.gray;

    if (event.detail.state === "SUCCESS") {
        color = colors.green;
    } else if (event.detail.state === "FAILURE") {
        color = colors.red;
    } else if (event.detail.state === "STOP") {
        color = colors.amber;
    }

    const attachment = [{
        "pretext": `Deployment for *${event.detail.application}* (${event.detail.deploymentGroup})`,
        "text": `Status: ${titleCase(event.detail.state)}`,
        "mrkdwn_in": ["pretext"],
        "color": color
    }];

    const slackMessage = {
        attachments: attachment,
        mrkdwn: true,
    };

    sendToSlack(slackMessage, webhooks.devops);
};

function titleCase(str) {
    return str.toLowerCase().split(' ').map(function(word) {
        return word.replace(word[0], word[0].toUpperCase());
    }).join(' ');
}
