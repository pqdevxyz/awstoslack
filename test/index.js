const sendToSlack = require('base/base.js');
const colors = require('base/colors.js');
const webhooks = require('base/webhooks.js');

exports.handler = (event, context, callback) => {
    let color = colors.gray;

    if (event.detail.severity < 4.0) {
        color = colors.yellow;
    } else if (event.detail.severity < 7.0) {
        color = colors.amber;
    } else if (event.detail.severity >= 7.0) {
        color = colors.red;
    }

    const attachment = [{
        "pretext": `*${event.detail.type}*`,
        "text": `${event.detail.description}`,
        "mrkdwn_in": ["pretext"],
        "color": color
    }];

    const slackMessage = {
        attachments: attachment,
        mrkdwn: true,
    };

    sendToSlack(slackMessage, webhooks.secops);
};
