const sendToSlack = require('base/base.js');
const colors = require('base/colors.js');
const webhooks = require('base/webhooks.js');

exports.handler = (event, context, callback) => {
    if (event.detail.state !== "COMPLETED" && event.detail.state !== "FAILED") {
        return;
    }

    let color = colors.gray;

    if (event.detail.state === "COMPLETED") {
        color = colors.green;
    } else if (event.detail.state === "FAILED") {
        color = colors.red;
    }

    const attachment = [{
        "pretext": `Backup for *${event.detail.resourceType}*`,
        "text": `Status: ${titleCase(event.detail.state)}`,
        "mrkdwn_in": ["pretext"],
        "color": color
    }];

    const slackMessage = {
        attachments: attachment,
        mrkdwn: true,
    };

    sendToSlack(slackMessage, webhooks.ops);
};

function titleCase(str) {
    return str.toLowerCase().split(' ').map(function(word) {
        return word.replace(word[0], word[0].toUpperCase());
    }).join(' ');
}
