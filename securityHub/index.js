const sendToSlack = require('base/base.js');
const colors = require('base/colors.js');
const webhooks = require('base/webhooks.js');

exports.handler = (event, context, callback) => {
    console.log(event.detail.findings);
    let color = colors.red;

    if (event.detail.findings.Compliance.Status !== "FAILED") {
        return;
    }

    if (event.detail.findings.Severity.Label !== 'CRITICAL' && event.detail.findings.Severity.Label !== 'HIGH') {
        return;
    }

    const attachment = [{
        "pretext": `Security Hub failed`,
        "text": `${event.detail.findings.Title}`,
        "mrkdwn_in": ["pretext"],
        "color": color
    }];

    const slackMessage = {
        attachments: attachment,
        mrkdwn: true,
    };

    sendToSlack(slackMessage, webhooks.secops);
};
