const sendToSlack = require('base/base.js');
const colors = require('base/colors.js');
const webhooks = require('base/webhooks.js');

exports.handler = (event, context, callback) => {
    let color = colors.gray;

    if (event.detail.state === "stopped") {
        color = colors.amber;
    } else if (event.detail.state === "terminated") {
        color = colors.red;
    } else if (event.detail.state === "running") {
        color = colors.green;
    }

    const attachment = [{
        "pretext": `EC2 state change for *${event.detail['instance-id']}*`,
        "text": `Status: ${titleCase(event.detail.state)}`,
        "mrkdwn_in": ["pretext"],
        "color": color
    }];

    const slackMessage = {
        attachments: attachment,
        mrkdwn: true,
    };

    sendToSlack(slackMessage, webhooks.ops);
};

function titleCase(str) {
    return str.toLowerCase().split(' ').map(function(word) {
        return word.replace(word[0], word[0].toUpperCase());
    }).join(' ');
}
