const https = require('https');

function send(message, url) {
    const data = JSON.stringify(message);
    const options = {
        port: 443,
        method: 'POST',
        headers: {
            'Content-Type': 'application/json',
            'Content-Length': data.length
        }
    }

    const req = https.request(url, options);

    req.on('error', error => {
        console.error(error)
    })

    req.write(data);
    req.end();
}

function sendToSlack(message, url) {
    send(message, url);
}

module.exports = function (message, url) {
    sendToSlack(message, url);
}
