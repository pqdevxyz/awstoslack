module.exports = {
    devops: process.env.devopsWebHook,
    secops: process.env.secopsWebHook,
    ops: process.env.opsWebHook,
}
