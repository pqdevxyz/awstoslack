module.exports = {
    gray: '#737373',
    red: '#ef4444',
    amber: '#f59e0b',
    yellow: '#eab308',
    lime: '#84cc16',
    green: '#22c55e',
    blue: '#3b82f6',
    sky: '#0ea5e9',
}
